# GRPC

* GRPC และ Protocol buffers คืออะไร
* GRPC มีข้อดีอย่างไร
* การพัฒนา GRPC ด้วย C#

## GRPC คืออะไร

gRPC (Google Protocol Remote Procedure Call) เป็น open source framework ที่ Google พัฒนาขึ้นมา เพื่อเรียกใช้คำสั่งข้าม Platforms โดยเป็นสื่อกลางระหว่างระบบต่าง ๆ โดยใช้ Protocol buffers เป็นภาษากลางสำหรับกำหนดโครงสร้างข้อมูล

## GRPC มีข้อดีอย่างไร

1. gRPC ใช้เทคโนโลยี Protobuf ซึ่งดีกว่า JSON และ XML 
1. gRPC ใช้การสื่อสารผ่านเทคโนโลยี HTTP/2 ซึ่งดีกว่า HTTP/1.1 
1. รองรับทั้งการทำงานทั้งแบบ synchronous และ asynchronous
1. รองรับหลาย languages และ platforms
1. รองรับ Bi-directional streaming และ integrated auth
1. รองรับการ Scale, load balancing, tracing, health check

## การพัฒนา GRPC ด้วย C#

* Required Tools
* การเตรียม Project
* การเตรียม Protocol buffers และกำหนดค่าการ Build

### Required Tools
1. Visual studio
1. .NETFramework 4.5 หรือ .NETStandard 1.5 ขึ้นไป

### การเตรียม Project
1. ติดตั้ง Package ดังนี้ ผ่าน NuGet

    1. Google.Protobuf
    1. GRPC
    1. GRPC.Tools

>![alt text](https://gitlab.com/sonarinno/focusone-grpc-document/raw/master/image/grpc-nuget.jpg "NuGet Project")

1. สร้างไฟล์ Protocol buffers ชื่อ FocusoneObject.proto

```protobuf
syntax = "proto3";

// The java definition
option java_multiple_files = true;
option java_package = "focusone.grpc.lib.ume.message.focusoneobject";
option java_outer_classname = "FocusOneObject";

// The objc definition
option objc_class_prefix = "FocusOneObject";

package focusone.grpc.lib.ume.message.focusoneobject;

service ObjectService {
	rpc GetFocusOneObject (ObjectRequest) returns (ObjectResponse) {}
	rpc GetStreamFocusOneObject (ObjectRequest) returns (stream ObjectResponse) {}
}

message ObjectRequest {
	string sid = 1;
	string username = 2;
	string role_id = 3;
}

message ObjectResponse {
	string response = 1;
}
```

ไฟล์ Protocol buffers จะประกอบด้วย 4 ส่วนหลัก คือ

1. syntax เป็นการกำหนด version ของ syntex ของ Protocol buffers ที่ใช้งาน
1. ส่วน // The java definition เป็นการกำหนด Package สำหรับ Java
1. ส่วน // The objc definition เป็นการกำหนด Package สำหรับ Objective C
1. package เป็นการกำหนดชื่อ Namespace
1. message คือข้อมูลที่ทำการรับส่ง โดย string หมายถึงชนิดของตัวแปร sid, username, role_id และ response คือชื่อตัวแปร และ Tag หลังเครื่องหมาย = หมายถึง Field Unique Number
1. service คือ Interface สำหรับรับและส่งข้อมูลผ่าน Protocol buffers

ศึกษาเพิ่มเติมได้ที่  
[https://developers.google.com/protocol-buffers/docs/proto3](https://developers.google.com/protocol-buffers/docs/proto3)  
[https://grpc.io/docs/quickstart/csharp/](https://grpc.io/docs/quickstart/csharp/)

3. กำหนด Build Action ของ FocusoneObject.proto เป็น Protobuf

![alt text](https://gitlab.com/sonarinno/focusone-grpc-document/raw/master/image/grpc-set-build.jpg "Set Build Action")

4. ทำการ Build ไฟล์ DLL เพื่อนำไปใช้งานต่อไป