# โครงสร้างระบบ

* การพัฒนาระบบ
* การทำงานของระบบ
* Messge Type

## การพัฒนาระบบ

![alt text](https://gitlab.com/sonarinno/focusone-grpc-document/raw/master/image/system-program.jpg "System-program")

การพัฒนาระบบประกอบด้วย 3 ส่วน คือ ส่วนของ FAPP ส่วนของ Protocol buffers และส่วนของ Application โดยส่วนของ Protocol buffers จะเป็นตัวเชื่อมระหว่าง FAPP และ Application ส่วนของ Protocol buffers ถูกสร้างโดยอ้างอิงจาก FAPP Library และจะถูก Build เป็น .DLL สำหรับให้ FAPP เรียกใช้งาน สำหรับการเรียกใช้งานโดย Application สามารถ Build เป็นไฟล์นามสกุลต่างๆ ได้ตามที่ Application ต้องการ

## การทำงานของระบบ

![alt text](https://gitlab.com/sonarinno/focusone-grpc-document/raw/master/image/system.jpg "System")

การทำงานของระบบประกอบด้วย 2 ส่วน คือ ส่วนของ FAPP และส่วนของ Web Application ส่วนของ Web Application ประกอบด้วย Web Server และ  Web Browser โดย Web Browser จะเชื่อมต่อกับ Web Server ผ่าน Protocol HTTP สำหรับส่วนของ Web Browser จะสามารถเชื่อมต่อกับส่วนของ FAPP ได้โดยตรงผ่านทาง Protocol GRPC

## Messge Type

เนื่องจากการส่งข้อมูลเป็นแบบ Stream ทั้งจาก Web ไปยัง FAPP และจาก FAPP กลับมายัง Web จึงได้ทำการแยกข้อมูลที่ส่งจาก Web ไปยัง FAPP เป็น Request และ Event ในขณะที่ข้อมูลที่ส่งกลับจาก FAPP มายัง Web เป็น Response และ Callback