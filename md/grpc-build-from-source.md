# gRPC C++ - Building from source on Windows

* Pre-requisites
* ปรับปรุง GRPC Core สำหรับ FAPP
* Build Grpc library
* Build Grpc.Core.dll

## Pre-requisites
ศึกษาวิธีการ build และเครื่องมือต่างๆ ที่ต้องใช้งานจาก [https://github.com/grpc/grpc/blob/master/BUILDING.md](https://github.com/grpc/grpc/blob/master/BUILDING.md)

## ปรับปรุง GRPC Core สำหรับ FAPP
1. ทำการปรับปรุงไฟล์ src/csharp/Grpc.Core/Internal/PlatformApis.cs ดังนี้

>```csharp
>            // Unity
>            //var unityApplicationClass = Type.GetType(UnityEngineApplicationClassName);
>
>            Type unityApplicationClass = null;
>            try
>            {
>                unityApplicationClass = Type.GetType(UnityEngineApplicationClassName);
>            } catch
>            {
>                unityApplicationClass = null;
>            }
>```

>```csharp
>            // Xamarin
>            //isXamarinIOS = Type.GetType(XamarinIOSObjectClassName) != null;
>            //isXamarinAndroid = Type.GetType(XamarinAndroidObjectClassName) != null;
>            //isXamarin = isXamarinIOS || isXamarinAndroid;
>
>            Type XamarinIOSObjectClass = null;
>            try
>            {
>                XamarinIOSObjectClass = Type.GetType(XamarinIOSObjectClassName);
>            } catch
>            {
>                XamarinIOSObjectClass = null;
>            }
>
>            Type XamarinAndroidObjectClass = null;
>            try
>            {
>                XamarinAndroidObjectClass = Type.GetType(XamarinAndroidObjectClassName);
>            }
>            catch
>            {
>                XamarinAndroidObjectClass = null;
>            }
>
>            isXamarinIOS = XamarinIOSObjectClass != null;
>            isXamarinAndroid = XamarinAndroidObjectClass != null;
>            isXamarin = isXamarinIOS || isXamarinAndroid;
>```

## Build Grpc.Core.dll
1. ทำการ Build ตามวิธีการใน [https://github.com/grpc/grpc/blob/master/BUILDING.md](https://github.com/grpc/grpc/blob/master/BUILDING.md)
1. ทำการ Copy ไฟล์ใน Folder ./build/Release ทั้งหมดไปยัง Folder cmake/build/x64/Debug

## Build Grpc.Core.dll
1. ทำการเปิด Project src/csharp/Grpccsproj ด้วย Visual Studio
1. ทำการ Build Project src/csharp/Grpc.Core.Api.csproj 
1. ทำการ Build Project src/csharp/Grpc.Core.csproj
1. นำไฟล์ Grpc.Core.dll ที่ได้จากการ build ไปใช้งานในส่วนของการพัฒนา FAPP