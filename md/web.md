# การพัฒนา Web

* Required Tools
* การเตรียม Project
* การเชื่อมต่อ GRPC เบื้องต้น

### Required Tools

1. Visual studio
1. .NET Core 3.0

### การเตรียม Project

1. สร้างโปรเจค .NET Core Web Application ด้วย Blazor

![alt text](https://gitlab.com/sonarinno/focusone-grpc-document/raw/master/image/web-select-WebApplication.jpg "blazor")

![alt text](https://gitlab.com/sonarinno/focusone-grpc-document/raw/master/image/web-select-blazor.jpg "blazor")

2. ติดตั้ง Package ดังนี้ ผ่าน NuGet
    1. Google.Protobuf
    1. GRPC
    1. GRPC.Tools

### การเชื่อมต่อ GRPC เบื้องต้น

1. เพิ่ม referece FocusOneObject.dll
1. แก้ไขไฟล์ _Import.razor
```csharp
@using System.Net.Http
@using Microsoft.AspNetCore.Authorization
@using Microsoft.AspNetCore.Components.Forms
@using Microsoft.AspNetCore.Components.Routing
@using Microsoft.JSInterop
@using BlazorGRPCWebClient
@using BlazorGRPCWebClient.Shared

@using System
@using System.Collections
@using Grpc.Core
@using Focusone.Lib.Grpc.Ume.Service.Roleprofilemanagerimpl
@using Focusone.Lib.Grpc.Ume.Message.Authobject
@using Focusone.Lib.Grpc.Common
@using Newtonsoft.Json
@using Google.Protobuf.Collections
```

3. แก้ไขไฟล์ Pages/Index.razor

```csharp
@page "/"

<h1>Hello, gRPC!</h1>

<form class="form form-horizontal">
    <div class="form-group">
        <label class="col-md-2 control-label">SID:</label>
        <div class="col-md-7">
            <input type="text" @bind="sId" class="form-control" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">Username:</label>
        <div class="col-md-7">
            <input type="text" @bind="username" class="form-control" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">RoleId:</label>
        <div class="col-md-7">
            <input type="text" @bind="roleId" class="form-control" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label"></label>
        <div class="col-md-7">
            <button type="button" class="btn btn-primary" @onclick="SendEvent">SendEvent</button>
            <button type="button" class="btn btn-primary" @onclick="GetStreamFocusOneObject">GetStreamFocusOneObject</button>
        </div>
    </div>
</form>

<hr />

<p>@ObjectRes</p>
<p>@StreamObjectRes</p>

@code {
    private string sId { get; set; } = "555";
    private string username { get; set; } = "focusone";
    private string roleId { get; set; } = "FICC_SM03";

    private string ObjectRes;
    private string StreamObjectRes;

    private AsyncDuplexStreamingCall<GetObjectByRoleIdRequest, AuthObjectResponse> call;

    //{ Sid = "555", Username = "focusone", RoleId = "FICC_SM03" }

    async Task SendEvent()
    {
        GetObjectByRoleIdRequest.Types.Event eventMessage = new GetObjectByRoleIdRequest.Types.Event
        {
            Type = "OnClickButton",
            Msg = "GetFocusOneObject"
        };

        GetObjectByRoleIdRequest even = new GetObjectByRoleIdRequest
        {
            MessageType = RequestMessageType.Event,
            Request = null,
            Event = eventMessage
        };

        await call.RequestStream.WriteAsync(even);
    }

    async Task GetStreamFocusOneObject()
    {
        //var endpoint = getFAPPEndpoint();
        //Channel channel = new Channel(endpoint, ChannelCredentials.Insecure);
        Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);
        //Channel channel = new Channel("192.168.99.100:50051", ChannelCredentials.Insecure);
        var client = new RoleProfileManagerImplService.RoleProfileManagerImplServiceClient(channel);

        GetObjectByRoleIdRequest.Types.Request reqMessage = new GetObjectByRoleIdRequest.Types.Request
        {
            Sid = sId,
            Username = username,
            RoleId = roleId
        };

        GetObjectByRoleIdRequest req = new GetObjectByRoleIdRequest
        {
            MessageType = RequestMessageType.Request,
            Request = reqMessage,
            Event = null
        };

        Metadata met = new Metadata();

        using (call = client.GetObjectByRoleId(met))
        {
            await call.RequestStream.WriteAsync(req);

            while (await call.ResponseStream.MoveNext())
            {
                Console.WriteLine(call.ResponseStream.Current);

                switch (call.ResponseStream.Current.MessageType)
                {
                    case ResponseMessageType.Response:
                        var responseTask = Task.Run(async () =>
                        {
                            await ResponseHandle(call.ResponseStream.Current.Response);
                        });
                        StateHasChanged();

                        break;
                    case ResponseMessageType.Callback:
                        var callbackTask = Task.Run(async () =>
                        {
                            await CallbackHandle(call.ResponseStream.Current.Callback);
                        });
                        StateHasChanged();

                        break;
                    default:
                        break;
                }
            }
        }

        await channel.ShutdownAsync();
    }

    string getFAPPEndpoint()
    {
        Console.WriteLine();
        Console.WriteLine("GetEnvironmentVariables: ");
        foreach (DictionaryEntry de in Environment.GetEnvironmentVariables(EnvironmentVariableTarget.Machine))
            Console.WriteLine("  {0} = {1}", de.Key, de.Value);

        // Check whether the environment variable exists.
        var endpoint = Environment.GetEnvironmentVariable("FAPP_ENDPOINT", EnvironmentVariableTarget.Machine);
        // If necessary, create it.
        if (endpoint == null)
        {
            endpoint = "127.0.0.1:50051";
        }
        // Display the value.
        Console.WriteLine($"Test1: {endpoint}\n");

        return endpoint;
    }

    async Task ResponseHandle(object response)
    {
        //Console.WriteLine(response);
        RepeatedField<AuthObjectResponse.Types.Response> res = response as RepeatedField<AuthObjectResponse.Types.Response>;

        string responseJson = JsonConvert.SerializeObject(res);
        StreamObjectRes = responseJson;

        await call.RequestStream.CompleteAsync();
    }

    async Task CallbackHandle(object callback)
    {
        AuthObjectResponse.Types.Callback res = callback as AuthObjectResponse.Types.Callback;

        string callbackJson = JsonConvert.SerializeObject(callback);
        StreamObjectRes = callbackJson;
    }
}
```

### ทดสอบ Run

1. ทำการเปิด FocusoneLicenseManager
1. เลือก Start/Stop Service
1. ทำการ Start Focusone Engine
1. ทำการ Run Web Application Project
1. กดปุ่ม GetStreamFocusOneObject เพื่อทำการส่ง Request 
1. กดปุ่ม SendEvent เพื่อทดสอบการส่ง Event

![alt text](https://gitlab.com/sonarinno/focusone-grpc-document/raw/master/image/web-run-blazor.jpg "blazor web application")