# การพัฒนา FAPP

* Required Tools
* การเตรียม Project
* โครงสร้าง GRPC
* การเชื่อมต่อ GRPC เบื้องต้น

### Required Tools

1. Visual studio
1. .NET Framwork 4.5 ขึ้นไป

### การเตรียม Project

1. ติดตั้ง Package ดังนี้ ผ่าน NuGet
    1. Grpc.Core.Api
    1. System.Interactive.Async
1. ติดตั้ง Grpc.Core.dll ซึ่งได้จากการ build GRPC source จาก [https://gitlab.com/sonarinno/focusone-grpc-document/wikis/GRPC-Build-For-FAPP](https://gitlab.com/sonarinno/focusone-grpc-document/wikis/GRPC-Build-For-FAPP) (ต้องติดตั้งไฟล์ grpc_csharp_ext.x**.dll สำหรับการเรียกใช้งาน Grpc.Core.dll หลังการ Build ด้วย)

### โครงสร้าง GRPC

![alt text](https://gitlab.com/sonarinno/focusone-grpc-document/raw/master/image/fapp-sturcture.jpg "fapp-structure")

ส่วนของ GRPC ทั้งหมดจะอยู่ใน Folder GRPC ของ FocusoneLicenseManager โดยภายในจะแยกออกเป็น 3 ส่วน หลักคือ

1. ส่วนของ Library ซึ่งอยู่ใน Folder Lib
1. ส่วนของ Common Service จะอยู่ใน Folder GRPC
1. ส่วนของ Librayr ที่ต้องการ Implement จะทำการแยก Folder ได้ตามโครงสร้างของ FAPP

### การเชื่อมต่อ GRPC เบื้องต้น

1. เพิ่ม referece Focusone.Lib.Grpc.dll
1. เพิ่ม Folder GRPC
1. เพิ่ม Folder UME
1. เพิ่มไฟล์ GRPC/UME/FocusOneObject.cs

```csharp
using System;

using AG.FLM.Remote.Proxy.UME.Bean;

namespace FocusOneLicenseManager
{
    class FocusOneObject
    {
        public AuthObject GetFocusOneObject(String sessionId, String sId, String username, String roleId)
        {
            Agape.Lib.UME.RoleProfileManagerImpl rpmi = new Agape.Lib.UME.RoleProfileManagerImpl();
            AuthObject obj = new AuthObject();
            AuthObject aObj = rpmi.getObjectByRoleId(sessionId, obj, roleId);

            return aObj;
        }
    }
}
```

4. เพิ่มไฟล์ GRPC/UME/RoleProfileManagerImpl.cs

```csharp
using System;
using System.Data;
using Grpc.Core;
using Focusone.Lib.Grpc.Common;
using Focusone.Lib.Grpc.Ume.Service.Roleprofilemanagerimpl;
using Focusone.Lib.Grpc.Ume.Message.Authobject;
using System.Threading.Tasks;

using Newtonsoft.Json;

using AG.FLM.Remote.Proxy.UME.Bean;
using AG.FLM.Remote.Proxy.UserSession;

using Google.Protobuf.Collections;
using Grpc.Core.Utils;

namespace FocusOneLicenseManager.GRPC.UME
{
    class RoleProfileManagerImpl : RoleProfileManagerImplService.RoleProfileManagerImplServiceBase
    {
        IServerStreamWriter<AuthObjectResponse> resStream;
        private GRPCCallbackSubscriber sub = null;

        Task requestTask;
        Task eventTask;

        public override async Task GetObjectByRoleId(IAsyncStreamReader<GetObjectByRoleIdRequest> requestStream, IServerStreamWriter<AuthObjectResponse> responseStream, ServerCallContext context)
        {
            resStream = responseStream;

            await requestStream.ForEachAsync(async request =>
            {
                //Console.WriteLine(requestStream.Current);
                switch (request.MessageType)
                {
                    case RequestMessageType.Request:
                        requestTask = Task.Run(async () =>
                        {
                            await RequestHandle(request.Request);
                        });

                        break;
                    case RequestMessageType.Event:
                        eventTask = Task.Run(async () =>
                        {
                            await EventHandle(request.Event);
                        });

                        break;
                    default:
                        break;
                }
            });
        }

        async Task RequestHandle(object request)
        {
            var req = request as GetObjectByRoleIdRequest.Types.Request;

            var sId = req.Sid;
            var username = req.Username;
            var roleId = req.RoleId;

            //var sId = "555";
            //var username = "focusone";
            //var roleId = "FICC_SM03";

            String terminalId = getTerminalId();
            String sessionId = getSessionId(sId, username, terminalId);

            GRPCCallbackSubscriber.Callback callback = new GRPCCallbackSubscriber.Callback(Callback);
            sub = new GRPCCallbackSubscriber(ref callback, sessionId);

            FocusOneObject focusOneObject = new FocusOneObject();
            AuthObject aObj = focusOneObject.GetFocusOneObject(sessionId, sId, username, roleId);

            AuthObjectResponse objResponse = new AuthObjectResponse
            {
                MessageType = ResponseMessageType.Response,
                Callback = null
            };

            //string json = JsonConvert.SerializeObject(aObj.BeanTable.DefaultView.Table);
            //Console.WriteLine(json);

            foreach (DataRow row in aObj.BeanTable.DefaultView.Table.Rows)
            {
                AuthObjectResponse.Types.Response res = new AuthObjectResponse.Types.Response
                {
                    Sid = row["sid"].ToString(),
                    ObjectId = row["objectid"].ToString(),
                    Description = row["description"].ToString(),
                    ProfileId = row["profileId"].ToString(),
                    ObjectType = row["objectType"].ToString(),
                    ObjectValue = row["objectValue"].ToString(),
                    ParentObjectId = row["parentObjectId"].ToString()
                };
                objResponse.Response.Add(res);
            }

            await resStream.WriteAsync(objResponse);

            sub.closeSocket();

            resetSessionId(sessionId, username, terminalId, sId);
        }

        async Task EventHandle(object even)
        {
            var e = even as GetObjectByRoleIdRequest.Types.Event;

            Console.WriteLine(e);
        }

        async Task Callback(int percentProgress, object userState)
        {
            //Console.WriteLine(percentProgress);
            //Console.WriteLine(userState);

            var callback = userState as string[];


            AuthObjectResponse.Types.Callback callbackMessage = new AuthObjectResponse.Types.Callback
            {
                Percent = percentProgress,
                Msg = callback[1],
                Msg2 = callback[2],
            };

            AuthObjectResponse callbackResponse = new AuthObjectResponse
            {
                MessageType = ResponseMessageType.Callback,
                Callback = callbackMessage
            };

            await resStream.WriteAsync(callbackResponse);
        }

        private String getTerminalId()
        {
            try
            {
                String _HostName = System.Net.Dns.GetHostName();
                System.Net.IPHostEntry IpHost = System.Net.Dns.GetHostEntry(_HostName);

                System.Net.IPAddress[] Addr = IpHost.AddressList;
                String _terminalId = _HostName + " - " + Addr[0].ToString();

                return _terminalId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static String getSessionId(String sId, String username, String terminalId)
        {
            try
            {
                RMUserSession RMU = new RMUserSession();
                String _sessionId = RMU.addSession(username, terminalId, sId, "", "");

                return _sessionId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void resetSessionId(String sessionId, String username, String terminalId, String sId)
        {
            try
            {
                RMUserSession RMU = new RMUserSession();
                RMU.deleteSession(sessionId, username, terminalId, sId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
```

5. เพิ่มไฟล์ GRPC/GRPCCallbackSubscriber.cs

```csharp
using System;
using System.Threading.Tasks;

using System.ComponentModel;

using System.Net;
using System.Net.Sockets;

using AG.FLM.Remote.Proxy.SYS.Bean;
using Agape.FocusOne.Utilities;

namespace FocusOneLicenseManager.GRPC
{
    public class GRPCCallbackSubscriber
    {
        const string address = "127.0.0.1";
        const Int32 port = 4500;

        IPEndPoint ipep;
        Socket server;
        BackgroundWorker bwReceiver;
        bool StartCallBack = true;
        private byte[] data = new byte[2048];
        string _sessionid = "";

        Callback _callback;

        public GRPCCallbackSubscriber(ref Callback callback, string p_sessionid)
        {
            Console.WriteLine("GRPCCallbackSubscriber");

            try
            {
                _sessionid = p_sessionid;
                StartCallBack = true;

                IPAddress[] addr = Dns.GetHostAddresses(address);
                IPAddress add2 = addr[0];
                foreach (IPAddress ip in addr)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        add2 = ip;
                        break;
                    }
                }

                _callback = callback;
                ipep = new IPEndPoint(add2, port + 20000);
                server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                server.Connect(ipep);

                CallBackMappingBean ObjData = new CallBackMappingBean();
                ObjData.FromName = p_sessionid;
                ObjData.ToName = "Sever";
                ObjData.Code = ApplicationConstants.CALLBACK_STATUS_CODE_LOGIN;
                ObjData.FromType = "Client";
                ObjData.ToType = "FAPPS";
                ObjData.Msg = p_sessionid;
                ObjData.CommandType = "Login";
                byte[] dataS = ObjData.GetDataByteArr();
                server.Send(dataS);

                bwReceiver = new BackgroundWorker();
                bwReceiver.WorkerSupportsCancellation = true;
                bwReceiver.DoWork += new DoWorkEventHandler(bwReceiver_DoWork);
                bwReceiver.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public delegate Task Callback(int percentProgress, object userState);

        void bwReceiver_DoWork(object sender, DoWorkEventArgs e)
        {
            while (StartCallBack)
            {
                try
                {
                    data = new byte[1024];
                    int receive = server.Receive(data);
                    byte[] TData = new byte[receive];
                    for (int i = 0; i < receive; i++)
                    {
                        TData[i] = data[i];
                    }
                    if (TData.Length > 0)
                    {
                        CallBackMappingBean ObjData = new CallBackMappingBean();
                        ObjData.LodeDataByteArr(TData);
                        switch (ObjData.Code)
                        {
                            default:
                                if (ObjData.Msg != "")
                                {
                                    try
                                    {
                                        _callback?.Invoke(ObjData.Percent, new string[] { ObjData.Percent.ToString(), ObjData.Msg, ObjData.Msg2 });
                                        
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine(ex.Message);
                                    }
                                }
                                break;
                        }
                    }
                }
                catch (SocketException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        public void closeSocket()
        {
            try
            {
                CallBackMappingBean ObjData = new CallBackMappingBean();
                ObjData.FromName = _sessionid;
                ObjData.ToName = "Sever";
                ObjData.Code = "50";
                ObjData.FromType = "Client";
                ObjData.ToType = "FAPPS";
                ObjData.Msg = _sessionid;
                ObjData.CommandType = "Login";
                byte[] dataS = ObjData.GetDataByteArr();
                server.Send(dataS);

                bwReceiver.CancelAsync();
                StartCallBack = false;
                //server.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void cancelSocket()
        {
            try
            {
                CallBackMappingBean ObjData = new CallBackMappingBean();
                ObjData.FromName = _sessionid;
                ObjData.ToName = "Sever";
                ObjData.Code = "51";
                ObjData.FromType = "Client";
                ObjData.ToType = "FAPPS";
                ObjData.Msg = _sessionid;
                ObjData.CommandType = "Login";
                byte[] dataS = ObjData.GetDataByteArr();
                server.Send(dataS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}

```

6. เพิ่มไฟล์ GRPC/GRPCServiceHelper.cs

```csharp
using System;
using Grpc.Core;

namespace FocusOneLicenseManager.GRPC
{
    public class GRPCServiceHelper
    {
        const string address = "localhost";
        const int port = 50051;

        Server server = null;

        public GRPCServiceHelper()
        {
            Console.WriteLine("StartGRPCService");

            try
            {
                server = new Server
                {
                    Services = {
                        Focusone.Lib.Grpc.Ume.Service.Roleprofilemanagerimpl.RoleProfileManagerImplService.BindService(new UME.RoleProfileManagerImpl())
                    },
                    Ports = { new ServerPort(address, port, ServerCredentials.Insecure) }
                };
                server.Start();
                Console.WriteLine("GRPC server listening on port " + port);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception encountered: {ex}");
            }
        }

        public void StopGRPCService()
        {
            Console.WriteLine("StopGRPCService");

            try
            {
                server?.ShutdownAsync().Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception encountered: {ex}");
            }
        }
    }
}
```

6. ปรังปรุงไฟล์ Service/ServiceManagementView.cs โดยเพิ่ม Code ดังด้านล่างลงใน function startService

```csharp
public partial class ServiceManagementView : UserControl, IObserver
    {
        ...
        GRPC.GRPCServiceHelper grpcServer = null;
        ...

        private void startService(String defaultPort,bool p_startWorker)
        ...
            grpcServer = new GRPC.GRPCServiceHelper();
        ...
```

### ทดสอบ Run

1. ทำการเปิด FocusoneLicenseManager
1. เลือก Start/Stop Service
1. ทำการ Start Focusone Engine

#### ผลการ Run

ส่วน Output ของ Visual Studio จะแสดงผลดังด้านล่าง
>GRPC server listening on port 50051