# เพิ่มความสามารถ Focusone ERP ด้วย GRPC

* [โครงสร้างระบบ](https://gitlab.com/sonarinno/focusone-grpc-document/wikis/System)
* [GRPC](https://gitlab.com/sonarinno/focusone-grpc-document/wikis/GRPC)
* [การสร้างไฟล์ Protocol buffers จาก FAPP Library](https://gitlab.com/sonarinno/focusone-grpc-document/wikis/GRPC-Lib-for-FAPP)
* [การพัฒนา FAPP ให้รองรับ GRPC](https://gitlab.com/sonarinno/focusone-grpc-document/wikis/FAPP)
* [การพัฒนา Web ให้รอบรับ GRPC](https://gitlab.com/sonarinno/focusone-grpc-document/wikis/Web)
