# การสร้างไฟล์ Protocol buffers (.proto) จาก FAPP Library

* โครงสร้าง Focusone-gRPC-Lib Project
* Syntax
* การสร้าง GRPC ส่วนของ Message จาก FAPP Library
* การสร้าง GRPC ส่วนของ Service จาก FAPP Library
* การ Build ด้วย Visual Studio
* การ Build ด้วย Script

## โครงสร้าง Focusone-gRPC-Lib Project

![alt text](https://gitlab.com/sonarinno/focusone-grpc-document/raw/master/image/proto-structure.jpg "Proto Project Structure")

Focusone-gRPC-Lib Project ประกอบด้วย 3 ส่วนหลัก คือ

1. Reference

ส่วนนี้ประกอบด้วย NuGet package ที่จำเป็นคือ
* Google.Protobuf
* GRPC
* GRPC.Tools

2. Folder common

ภายในคือไฟล์ common ที่จะถูกใช้ในทุกๆ Message

3. Folder template

ภายในคือไฟล์ template สำหรับใช้ในการสร้าง GRPC ส่วนของ Message และ Service จาก FAPP Library

## Syntax

1. ทำการสร้าง Folder สำหรับ FAPP Library ด้วยตัวอักษร พิมพ์เล็กเท่านั้น
2. ภายใน Folder จะประกอบด้วย Folder messages และ/หรือ services 
3. ภายใน Folder messages คือส่วนของ Bean จาก FAPP 
4. ภายใน Folder services คือส่วนของ Function สำหรับเรียกใช้งานในส่วนของ Lib
5. การตั้งชื่อไฟล์ใน Folder messages ให้ตั้งชื่อตาม Class โดยขึ้นต้นด้วยตัวอักษรพิมพ์ใหญ่
6. การตั้งชื่อไฟล์ใน Folder services ให้ตั้งชื่อตาม Class โดยขึ้นต้นด้วยตัวอักษรพิมพ์ใหญ่
7. การกำหนดชื่อ Package ด้านในไฟล์ .proto ให้ใช้ pattern ดังนี้

Pattern สำหรับ message

```protobuf
// The java definition
option java_multiple_files = true;
option java_package = "focusone.grpc.lib.ume.message.authprofiles";
option java_outer_classname = "AuthProfilesMessage";

// The objc definition
option objc_class_prefix = "AuthProfilesMessage";

package focusone.grpc.lib.ume.message.authprofiles;
```

Pattern สำหรับ service

```protobuf
// The java definition
option java_multiple_files = true;
option java_package = "focusone.grpc.lib.ume.service.roleprofilemanagerimpl";
option java_outer_classname = "RoleProfileManagerImplService";

// The objc definition
option objc_class_prefix = "RoleProfileManagerImplService";

package focusone.grpc.lib.ume.service.roleprofilemanagerimpl;
```

*** Syntax เป็นแค่แนวทางเท่านั้น สามารถปรับเปลียนได้ตามความเหมาะสม 

## การสร้าง GRPC ส่วนของ Message จาก FAPP Library

ในที่นี้จะยกตัวอย่างการสร้าง Message สำหรับ FAPP Library UME บางส่วน โดยอย่างอิงจากไฟล์ DLL ดังภาพ

![alt text](https://gitlab.com/sonarinno/focusone-grpc-document/raw/master/image/proto-authobject-structure.jpg "proto-authobject-structure")

1. ทำการสร้าง Folder ume (ถ้ายังไม่มี)
1. ทำการสร้าง Folder messages ภายใน Folder ume
1. ทำการสร้างไฟล์ AuthObject.proto ดังนี้

```protobuf
syntax = "proto3";

import "common/common.proto";

// The java definition
option java_multiple_files = true;
option java_package = "focusone.grpc.lib.ume.message.authobject";
option java_outer_classname = "AuthObjectMessage";

// The objc definition
option objc_class_prefix = "AuthObjectMessage";

package focusone.grpc.lib.ume.message.authobject;

message AuthObjectResponse {
	message Response {
		string description = 1;
		string object_id = 2;
		string object_type = 3;
		string object_value = 4;
		string parent_object_id = 5;
		string profile_id = 6;
		string sid = 7;
	}

	message Callback {
		int32 percent = 1;
		string msg = 2;
		string msg2 = 3;
	}

	focusone.grpc.lib.common.ResponseMessageType message_type = 1;
	repeated Response response = 2;
	Callback callback = 3;
}
```

***Note***

```protobuf
repeated Response response = 2;
```

เป็นการสร้างตัวแบบแบบ List

3. กำหนด Build Action ของ AuthObject.proto เป็น Protobuf

## การสร้าง GRPC ส่วนของ Service จาก FAPP Library

ในที่นี้จะยกตัวอย่างการสร้าง Service สำหรับ FAPP Library UME บางส่วน โดยอย่างอิงจากไฟล์ DLL ดังภาพ

![alt text](https://gitlab.com/sonarinno/focusone-grpc-document/raw/master/image/proto-roleprofilemanagerimpl-structure.jpg "proto-roleprofilemanagerimpl-structure")

1. ทำการสร้าง Folder ume (ถ้ายังไม่มี)
1. ทำการสร้าง Folder services ภายใน Folder ume
1. ทำการสร้างไฟล์ RoleProfileManagerImpl.proto ดังนี้

```protobuf
syntax = "proto3";

import "common/common.proto";
import "ume/messages/AuthObject.proto";
import "ume/messages/AuthProfiles.proto";

// The java definition
option java_multiple_files = true;
option java_package = "focusone.grpc.lib.ume.service.roleprofilemanagerimpl";
option java_outer_classname = "RoleProfileManagerImplService";

// The objc definition
option objc_class_prefix = "RoleProfileManagerImplService";

package focusone.grpc.lib.ume.service.roleprofilemanagerimpl;

message  GetObjectByRoleIdRequest {

	message Request {
		string sid = 1;
		string username = 2;
		string role_id = 3;
		string object_type = 4;
	}

	message Event {
		string type = 1;
		string msg = 2;
	}

	focusone.grpc.lib.common.RequestMessageType message_type = 1;
	Request request = 2;
	Event event = 3;
}

message  GetProfilesByRoleIdRequest {

	message Request {
		string session_id = 1;
		string username = 2;
		string role_id = 3;
		string object_type = 4;
	}

	message Event {
		string type = 1;
		string msg = 2;
	}

	focusone.grpc.lib.common.RequestMessageType message_type = 1;
	Request request = 2;
	Event event = 3;
}

service RoleProfileManagerImplService {
	rpc GetObjectByRoleId (stream GetObjectByRoleIdRequest) returns (stream focusone.grpc.lib.ume.message.authobject.AuthObjectResponse) {}
	rpc GetProfilesByRoleId (stream  GetProfilesByRoleIdRequest) returns (stream focusone.grpc.lib.ume.message.authprofiles.AuthProfilesResponse) {}
}
```
3. กำหนด Build Action ของ RoleProfileManagerImpl.proto เป็น Protobuf

## การ Build ด้วย Visual Studio

ทำการ Build Solution ด้วย Visual Studio แล้วนำไฟล์ Focusone-gRPC-Lib.dll ไปใช้งาน

## การ Build ด้วย Script
Coming Soon

